From: Ritesh Raj Sarraf <ritesh.sarraf@collabora.com>
Date: Fri, 17 Jan 2020 20:13:27 +0530
Subject: Add in rollback generation

We wish to provide a second config that can be used in the event that the
latest upgrade fails to boot successfully (to be determined by system
specific metrics). Cause OSTree to generate an alternative config that
defaults to the alternative OSTree image that the bootloader can swtich to
when a failure state is detected.

Signed-off-by: Martyn Welch <martyn.welch@collabora.co.uk>
Signed-off-by: Ritesh Raj Sarraf <ritesh.sarraf@collabora.com>
---
 src/libostree/ostree-bootloader-uboot-distro.c | 59 ++++++++++++++++++++------
 1 file changed, 47 insertions(+), 12 deletions(-)

--- a/src/libostree/ostree-bootloader-uboot-distro.c
+++ b/src/libostree/ostree-bootloader-uboot-distro.c
@@ -62,6 +62,7 @@
 static gboolean
 append_config_from_boostree_loader_entries (OstreeBootloaderUbootDistro  *self,
                                         gboolean               regenerate_default,
+					gboolean               generate_rollback,
                                         int                    bootversion,
                                         GPtrArray             *new_lines,
                                         GCancellable          *cancellable,
@@ -70,27 +71,39 @@
   gboolean ret = FALSE;
   g_autoptr(GPtrArray) boostree_loader_configs = NULL;
   guint i;
+  OstreeBootconfigParser *config;
+  const char *val;
 
   if (!_ostree_sysroot_read_boot_loader_configs (self->sysroot, bootversion, &boostree_loader_configs,
                                                  cancellable, error))
     goto out;
 
-  for (i = 0; i < boostree_loader_configs->len; i++)
+  if (regenerate_default && boostree_loader_configs->len > 0)
     {
-      OstreeBootconfigParser *config = boostree_loader_configs->pdata[i];
-      const char *val;
+      guint default_item = 0;
+
+      if (generate_rollback && boostree_loader_configs->len > 1)
+        default_item = 1;
+
+      config = boostree_loader_configs->pdata[default_item];
 
       val = ostree_bootconfig_parser_get (config, "title");
       if (!val)
         val = "(Untitled)";
 
-      if (regenerate_default && i == 0)
-        {
-          g_ptr_array_add (new_lines, g_strdup_printf ("default %s\n", val));
-        }
+      g_ptr_array_add (new_lines, g_strdup_printf ("default %s\n", val));
+    }
+
+  for (i = 0; i < boostree_loader_configs->len; i++)
+    {
+      config = boostree_loader_configs->pdata[i];
+
+      val = ostree_bootconfig_parser_get (config, "title");
+      if (!val)
+        val = "(Untitled)";
 
       g_ptr_array_add (new_lines, g_strdup_printf ("label %s", val));
-      
+ 
       val = ostree_bootconfig_parser_get (config, "linux");
       if (!val)
         {
@@ -219,10 +232,10 @@
   return iter;
 }
 
-
 static gboolean
-_ostree_bootloader_uboot_distro_write_config (OstreeBootloader          *bootloader,
+_ostree_bootloader_uboot_distro_write_one_config (OstreeBootloader          *bootloader,
                                      int                    bootversion,
+                                     gboolean               generate_rollback,
                                      GCancellable          *cancellable,
                                      GError               **error)
 {
@@ -237,8 +250,12 @@
   char **lines = NULL;
   char **iter;
 
-  new_config_path = ot_gfile_resolve_path_printf (self->sysroot->path, "boot/loader.%d/extlinux.conf",
-                                                  bootversion);
+  if (generate_rollback)
+    new_config_path = ot_gfile_resolve_path_printf (self->sysroot->path, "boot/loader.%d/extlinux-rollback.conf",
+                                                    bootversion);
+  else
+    new_config_path = ot_gfile_resolve_path_printf (self->sysroot->path, "boot/loader.%d/extlinux.conf",
+                                                    bootversion);
 
   /* This should follow the symbolic link to the current bootversion. */
   config_contents = glnx_file_get_contents_utf8_at (AT_FDCWD, gs_file_get_path_cached (self->config_path), NULL,
@@ -286,6 +303,7 @@
     }
 
   if (!append_config_from_boostree_loader_entries (self, regenerate_default,
+                                               generate_rollback,
                                                bootversion, new_lines,
                                                cancellable, error))
     goto out;
@@ -307,6 +325,23 @@
   return ret;
 }
 
+static gboolean
+_ostree_bootloader_uboot_distro_write_config (OstreeBootloader          *bootloader,
+                                     int                    bootversion,
+                                     GPtrArray             *new_deployments,
+                                     GCancellable          *cancellable,
+                                     GError               **error)
+{
+  gboolean ret;
+
+  ret = _ostree_bootloader_uboot_distro_write_one_config (bootloader, bootversion, FALSE, cancellable, error);
+  if (!ret)
+    return ret;
+
+  return _ostree_bootloader_uboot_distro_write_one_config (bootloader, bootversion, TRUE, cancellable, error);
+
+}
+
 static void
 _ostree_bootloader_uboot_distro_finalize (GObject *object)
 {
