From: Ritesh Raj Sarraf <ritesh.sarraf@collabora.com>
Date: Fri, 17 Jan 2020 20:05:01 +0530
Subject: tests: sysroot: Test deployment of dtb files

There are two conventions for the layout of the dtb directory, one is to
just have all the dtbs at the top-level (e.g. done on 32 bit arm) the
other is to have them in subdirectories per vendor (e.g. done on 64 bit
arm). Extend the test for dtb using both strategies and while there also
check the initramfs is all happy.

Signed-off-by: Sjoerd Simons <sjoerd.simons@collabora.co.uk>
Signed-off-by: Ritesh Raj Sarraf <ritesh.sarraf@collabora.com>
---
 tests/admin-test.sh |  3 +++
 tests/libtest.sh    | 10 ++++++++--
 2 files changed, 11 insertions(+), 2 deletions(-)

--- a/tests/admin-test.sh
+++ b/tests/admin-test.sh
@@ -80,6 +80,9 @@
 assert_file_has_content sysroot/boot/loader/entries/ostree-1-testos.conf 'options.* root=LABEL=MOO'
 assert_file_has_content sysroot/boot/loader/entries/ostree-1-testos.conf 'options.* quiet'
 assert_file_has_content sysroot/boot/ostree/testos-${bootcsum}/vmlinuz-3.6.0 'a kernel'
+assert_file_has_content sysroot/boot/ostree/testos-${bootcsum}/initramfs-3.6.0.img 'an initramfs'
+assert_file_has_content sysroot/boot/ostree/testos-${bootcsum}/dtbs-3.6.0/board.dtb 'a dtb'
+assert_file_has_content sysroot/boot/ostree/testos-${bootcsum}/dtbs-3.6.0/vendor/board.dtb 'another dtb'
 assert_file_has_content sysroot/ostree/deploy/testos/deploy/${rev}.0/etc/os-release 'NAME=TestOS'
 assert_file_has_content sysroot/ostree/boot.1/testos/${bootcsum}/0/etc/os-release 'NAME=TestOS'
 assert_ostree_deployment_refs 1/1/0
--- a/tests/libtest.sh
+++ b/tests/libtest.sh
@@ -399,6 +399,7 @@
     mkdir -p usr/bin ${bootdir} usr/lib/modules/${kver} usr/share usr/etc
     kernel_path=${bootdir}/vmlinuz
     initramfs_path=${bootdir}/initramfs.img
+    dtbs_path=${bootdir}/dtbs
     # the HMAC file is only in /usr/lib/modules
     hmac_path=usr/lib/modules/${kver}/.vmlinuz.hmac
     # /usr/lib/modules just uses "vmlinuz", since the version is in the module
@@ -406,17 +407,22 @@
     if [[ $bootdir != usr/lib/modules/* ]]; then
         kernel_path=${kernel_path}-${kver}
         initramfs_path=${bootdir}/initramfs-${kver}.img
+        dtbs_path=${bootdir}/dtbs-${kver}
     fi
     echo "a kernel" > ${kernel_path}
     echo "an initramfs" > ${initramfs_path}
     echo "an hmac file" > ${hmac_path}
-    bootcsum=$(cat ${kernel_path} ${initramfs_path} | sha256sum | cut -f 1 -d ' ')
+    mkdir -p ${dtbs_path}/vendor
+    echo "a dtb" > ${dtbs_path}/board.dtb
+    echo "another dtb" > ${dtbs_path}/vendor/board.dtb
+    bootcsum=$(cat ${kernel_path} ${initramfs_path} ${dtbs_path}/board.dtb ${dtbs_path}/vendor/board.dtb | sha256sum | cut -f 1 -d ' ')
     export bootcsum
     # Add the checksum for legacy dirs (/boot, /usr/lib/ostree-boot), but not
     # /usr/lib/modules.
     if [[ $bootdir != usr/lib/modules/* ]]; then
         mv ${kernel_path}{,-${bootcsum}}
         mv ${initramfs_path}{,-${bootcsum}}
+        mv ${dtbs_path}{,-${bootcsum}}
     fi
 
     echo "an executable" > usr/bin/sh
@@ -511,7 +517,7 @@
             bootdir=boot
         fi
     fi
-    rm ${bootdir}/*
+    rm -r ${bootdir}/*
     kernel_path=${bootdir}/vmlinuz
     initramfs_path=${bootdir}/initramfs.img
     if [[ $bootdir != usr/lib/modules/* ]]; then
