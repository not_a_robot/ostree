Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: 2011-2018, Colin Walters <walters@verbum.org>
License: LGPL-2+

Files: Makefile-bash.am
Copyright: 2017, Red Hat Inc.
License: LGPL-2+

Files: Makefile.in
Copyright: 1994-2018, Free Software Foundation, Inc.
License: LGPL-2+

Files: apidoc/Makefile.in
Copyright: 1994-2018, Free Software Foundation, Inc.
License: LGPL-2+

Files: bsdiff/*
Copyright: no-info-found
License: BSD-2-clause

Files: bsdiff/Makefile-bsdiff.am
 bsdiff/Makefile-bsdiff.am.inc
Copyright: 2015, Giuseppe Scrivano <gscrivan@redhat.com>
License: BSD-2-clause

Files: bsdiff/bsdiff.c
 bsdiff/bsdiff.h
 bsdiff/bspatch.c
 bsdiff/bspatch.h
Copyright: no-info-found
License: BSD-2-clause

Files: build-aux/*
Copyright: 1996-2018, Free Software Foundation, Inc.
License: GPL-2+

Files: build-aux/install-sh
Copyright: 1994, X Consortium
License: Expat

Files: buildutil/attributes.m4
Copyright: no-info-found
License: GPL-2+

Files: buildutil/libtool.m4
Copyright: 1996-2001, 2003-2015, Free Software Foundation, Inc.
License: GPL

Files: buildutil/tap-driver.sh
Copyright: 1996-2018, Free Software Foundation, Inc.
License: GPL-2+

Files: configure
Copyright: 1992-1996, 1998-2012, Free Software Foundation, Inc.
License: FSFUL

Files: debian/*
Copyright: no-info-found
License: LGPL-2+

Files: debian/ostree-boot-examples/deb-ostree-builder
Copyright: 2017, Dan Nicholson <nicholson@endlessm.com>
License: GPL-2+

Files: debian/ostree-boot-examples/modified-deb-ostree-builder
Copyright: no-info-found
License: GPL-2+

Files: debian/patches/Add-bootloader-configuration-for-sd-boot.patch
Copyright: 2013, Collabora Ltd
License: LGPL-2

Files: debian/patches/static-delta-sign/0009-tests-delta-new-ed25519-tests-for-signing-and-multip.patch
Copyright: 2011-2018, Colin Walters <walters@verbum.org>
License: LGPL-2+

Files: debian/patches/u-boot-distro-configuration-loader.patch
Copyright: no-info-found
License: LGPL-2

Files: libglnx/Makefile-libglnx.am
 libglnx/Makefile-libglnx.am.inc
 libglnx/glnx-local-alloc.c
Copyright: 2011-2018, Colin Walters <walters@verbum.org>
License: LGPL-2+

Files: libglnx/glnx-backport-autocleanups.h
Copyright: 2015, Canonical Limited
License: LGPL-2

Files: libglnx/glnx-backport-autoptr.h
 libglnx/glnx-backports.h
Copyright: no-info-found
License: LGPL-2+

Files: libglnx/glnx-backports.c
 libglnx/glnx-console.c
 libglnx/glnx-console.h
Copyright: 2013-2015, 2019, Colin Walters <walters@verbum.org>
License: LGPL-2

Files: libglnx/glnx-dirfd.c
 libglnx/glnx-dirfd.h
 libglnx/glnx-errors.c
 libglnx/glnx-errors.h
 libglnx/glnx-fdio.h
 libglnx/glnx-local-alloc.h
 libglnx/glnx-shutil.c
 libglnx/glnx-shutil.h
 libglnx/glnx-xattrs.c
 libglnx/glnx-xattrs.h
 libglnx/libglnx.h
Copyright: 2011-2015, Colin Walters <walters@verbum.org>.
License: LGPL-2+

Files: libglnx/glnx-fdio.c
Copyright: no-info-found
License: LGPL-2+

Files: libglnx/glnx-lockfile.c
 libglnx/glnx-lockfile.h
Copyright: no-info-found
License: LGPL-2.1+

Files: libglnx/glnx-macros.h
Copyright: no-info-found
License: LGPL-2+

Files: libglnx/glnx-missing-syscall.h
Copyright: no-info-found
License: LGPL-2.1+

Files: libglnx/glnx-missing.h
Copyright: 2010, Lennart Poettering
License: LGPL-2.1+

Files: libglnx/tests/*
Copyright: 2015-2018, 2020, Red Hat, Inc.
License: LGPL-2+

Files: libglnx/tests/libglnx-testlib.c
Copyright: 2019, 2020, Collabora Ltd.
License: LGPL-2+

Files: libglnx/tests/libglnx-testlib.h
Copyright: no-info-found
License: LGPL-2+

Files: libglnx/tests/test-libglnx-shutil.c
Copyright: 2016-2019, Endless Mobile, Inc.
License: LGPL-2+

Files: man/*
Copyright: 2014, Anne LoVerso <anne.loverso@students.olin.edu>
License: LGPL-2+

Files: man/ostree-admin-pin.xml
Copyright: 2015, 2016, 2018, Red Hat
License: LGPL-2+

Files: man/ostree-admin-set-origin.xml
 man/ostree-admin-unlock.xml
 man/ostree-export.xml
 man/ostree-summary.xml
 man/ostree-trivial-httpd.xml
 man/ostree.repo-config.xml
 man/ostree.repo.xml
 man/ostree.xml
 man/rofiles-fuse.xml
Copyright: 2011-2018, Colin Walters <walters@verbum.org>
License: LGPL-2+

Files: man/ostree-create-usb.xml
 man/ostree-find-remotes.xml
Copyright: 2018, Matthew Leeds <matthew.leeds@endlessm.com>
License: LGPL-2+

Files: man/ostree-gpg-sign.xml
Copyright: 2015, Matthew Barnes <mbarnes@redhat.com>
License: LGPL-2+

Files: man/ostree-sign.xml
Copyright: 2019, Denis Pynkin <denis.pynkin@collabora.com>
License: LGPL-2+

Files: src/boot/grub2/*
Copyright: 2013-2015, 2019, Colin Walters <walters@verbum.org>
License: LGPL-2

Files: src/boot/ostree-finalize-staged.path
 src/boot/ostree-finalize-staged.service
Copyright: 2015-2018, 2020, Red Hat, Inc.
License: LGPL-2+

Files: src/libostree/bupsplit.c
 src/libostree/bupsplit.h
Copyright: 2011, Avery Pennarun.
License: BSD-2-clause

Files: src/libostree/ostree-autocleanups.h
 src/libostree/ostree-bloom-private.h
 src/libostree/ostree-bloom.c
 src/libostree/ostree-ref.c
 src/libostree/ostree-ref.h
 src/libostree/ostree-repo-finder-avahi-private.h
 src/libostree/ostree-repo-finder-avahi.h
 src/libostree/ostree-repo-finder-config.c
 src/libostree/ostree-repo-finder-config.h
 src/libostree/ostree-repo-finder-mount.c
 src/libostree/ostree-repo-finder-mount.h
 src/libostree/ostree-repo-finder-override.c
 src/libostree/ostree-repo-finder-override.h
 src/libostree/ostree-repo-finder.c
 src/libostree/ostree-repo-finder.h
 src/libostree/ostree-repo-pull-private.h
Copyright: 2016-2019, Endless Mobile, Inc.
License: LGPL-2+

Files: src/libostree/ostree-bootconfig-parser.c
 src/libostree/ostree-bootconfig-parser.h
 src/libostree/ostree-bootloader-grub2.c
 src/libostree/ostree-bootloader-grub2.h
 src/libostree/ostree-bootloader-syslinux.c
 src/libostree/ostree-bootloader-syslinux.h
 src/libostree/ostree-bootloader-uboot-distro.h
 src/libostree/ostree-bootloader-zipl.c
 src/libostree/ostree-bootloader-zipl.h
 src/libostree/ostree-bootloader.c
 src/libostree/ostree-bootloader.h
 src/libostree/ostree-deployment-private.h
 src/libostree/ostree-deployment.c
 src/libostree/ostree-deployment.h
 src/libostree/ostree-kernel-args.c
 src/libostree/ostree-kernel-args.h
Copyright: 2013-2015, 2019, Colin Walters <walters@verbum.org>
License: LGPL-2

Files: src/libostree/ostree-bootloader-sd-boot.c
 src/libostree/ostree-bootloader-uboot.c
 src/libostree/ostree-bootloader-uboot.h
Copyright: 2013, Collabora Ltd
License: LGPL-2

Files: src/libostree/ostree-bootloader-sd-boot.h
Copyright: no-info-found
License: LGPL-2

Files: src/libostree/ostree-bootloader-uboot-distro.c
Copyright: no-info-found
License: LGPL-2

Files: src/libostree/ostree-dummy-enumtypes.c
 src/libostree/ostree-dummy-enumtypes.h
 src/libostree/ostree-enumtypes.c.template
 src/libostree/ostree-enumtypes.h.template
 src/libostree/ostree-gpg-verify-result-private.h
 src/libostree/ostree-gpg-verify-result.c
 src/libostree/ostree-gpg-verify-result.h
 src/libostree/ostree-repo-deprecated.h
Copyright: 2015-2018, 2020, Red Hat, Inc.
License: LGPL-2+

Files: src/libostree/ostree-gpg-verifier.c
 src/libostree/ostree-gpg-verifier.h
Copyright: no-info-found
License: LGPL-2+

Files: src/libostree/ostree-gpg-verify-result-dummy.c
Copyright: no-info-found
License: LGPL-2+

Files: src/libostree/ostree-lzma-common.h
 src/libostree/ostree-lzma-decompressor.h
Copyright: 2014, Colin Walters <walters@redhat.com>
License: LGPL-2+

Files: src/libostree/ostree-remote-private.h
 src/libostree/ostree-remote.c
 src/libostree/ostree-remote.h
Copyright: no-info-found
License: LGPL-2+

Files: src/libostree/ostree-repo-finder-avahi-parser.c
 src/libostree/ostree-repo-finder-avahi.c
Copyright: no-info-found
License: LGPL-2+

Files: src/libostree/ostree-repo-pull-verify.c
Copyright: no-info-found
License: LGPL-2+

Files: src/libostree/ostree-repo-pull.c
Copyright: no-info-found
License: LGPL-2+

Files: src/libostree/ostree-repo.c
Copyright: no-info-found
License: LGPL-2+

Files: src/libostree/ostree-sign-dummy.c
 src/libostree/ostree-sign-dummy.h
 src/libostree/ostree-sign-ed25519.c
 src/libostree/ostree-sign-ed25519.h
 src/libostree/ostree-sign.c
 src/libostree/ostree-sign.h
Copyright: 2019, 2020, Collabora Ltd.
License: LGPL-2+

Files: src/libostree/ostree-varint.c
Copyright: 2013, Colin Walters <walters@verbum.org>
License: BSD-3-clause or LGPL-2+

Files: src/libostree/ostree-version.h
 src/libostree/ostree-version.h.in
Copyright: 2017, Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
License: LGPL-2

Files: src/libotutil/ot-checksum-utils.h
 src/libotutil/ot-fs-utils.h
 src/libotutil/ot-gio-utils.h
 src/libotutil/ot-keyfile-utils.h
 src/libotutil/ot-opt-utils.h
 src/libotutil/ot-unix-utils.h
 src/libotutil/ot-variant-utils.h
 src/libotutil/otutil.h
Copyright: 2011-2015, Colin Walters <walters@verbum.org>.
License: LGPL-2+

Files: src/libotutil/ot-gpg-utils.c
 src/libotutil/ot-gpg-utils.h
Copyright: 2015-2018, 2020, Red Hat, Inc.
License: LGPL-2+

Files: src/libotutil/ot-variant-builder.c
 src/libotutil/ot-variant-builder.h
Copyright: 2017, Alexander Larsson <alexl@redhat.com>.
License: LGPL-2+

Files: src/ostree/ot-admin-builtin-finalize-staged.c
 src/ostree/ot-remote-builtin-add.c
 src/ostree/ot-remote-builtin-delete.c
 src/ostree/ot-remote-builtin-gpg-import.c
 src/ostree/ot-remote-builtin-list.c
 src/ostree/ot-remote-builtin-refs.c
 src/ostree/ot-remote-builtin-show-url.c
 src/ostree/ot-remote-builtin-summary.c
 src/ostree/ot-remote-builtins.h
Copyright: 2015-2018, 2020, Red Hat, Inc.
License: LGPL-2+

Files: src/ostree/ot-admin-instutil-builtin-grub2-generate.c
 src/ostree/ot-admin-instutil-builtin-selinux-ensure-labeled.c
 src/ostree/ot-admin-instutil-builtin-set-kargs.c
Copyright: 2013-2015, 2019, Colin Walters <walters@verbum.org>
License: LGPL-2

Files: src/ostree/ot-builtin-create-usb.c
 src/ostree/ot-builtin-find-remotes.c
Copyright: 2016-2019, Endless Mobile, Inc.
License: LGPL-2+

Files: src/ostree/ot-builtin-log.c
 src/ostree/ot-builtin-reset.c
 src/ostree/ot-dump.h
 src/ostree/ot-editor.c
 src/ostree/ot-editor.h
Copyright: 2013, Stef Walter <stefw@redhat.com>
License: LGPL-2+

Files: src/ostree/ot-builtin-sign.c
Copyright: no-info-found
License: LGPL-2+

Files: src/ostree/ot-dump.c
Copyright: no-info-found
License: LGPL-2+

Files: src/ostree/ot-remote-builtin-add-cookie.c
 src/ostree/ot-remote-builtin-delete-cookie.c
 src/ostree/ot-remote-builtin-list-cookies.c
 src/ostree/ot-remote-cookie-util.c
Copyright: no-info-found
License: LGPL-2+

Files: src/ostree/parse-datetime.h
Copyright: 1995, 1997, 1998, 2003, 2004, 2007, 2009-2015, Free Software
License: LGPL-2+

Files: src/ostree/parse-datetime.y
Copyright: 1994-2018, Free Software Foundation, Inc.
License: LGPL-2+

Files: src/switchroot/ostree-prepare-root.c
Copyright: no-info-found
License: LGPL-2+

Files: tests/bootloader-entries-crosscheck.py
 tests/test-pull-repeated.sh
Copyright: 2015, 2016, 2018, Red Hat
License: LGPL-2+

Files: tests/libostreetest.c
 tests/test-admin-deploy-nomerge.sh
 tests/test-basic-c.c
 tests/test-bsdiff.c
 tests/test-checksum.c
 tests/test-gpg-verify-result.c
 tests/test-kargs.c
 tests/test-keyfile-utils.c
 tests/test-libarchive-import.c
 tests/test-lzma.c
 tests/test-mutable-tree.c
 tests/test-oldstyle-partial.sh
 tests/test-ot-opt-utils.c
 tests/test-ot-tool-util.c
 tests/test-ot-unix-utils.c
 tests/test-prune.sh
 tests/test-pull-c.c
 tests/test-pull-commit-only.sh
 tests/test-pull-contenturl.sh
 tests/test-pull-localcache.sh
 tests/test-pull-mirrorlist.sh
 tests/test-refs.sh
 tests/test-remote-gpg-import.sh
 tests/test-remote-headers.sh
 tests/test-rollsum.c
 tests/test-sysroot-c.c
Copyright: 2015-2018, 2020, Red Hat, Inc.
License: LGPL-2+

Files: tests/readdir-rand.c
Copyright: 2011-2015, Colin Walters <walters@verbum.org>.
License: LGPL-2+

Files: tests/repo-finder-mount.c
 tests/test-bloom.c
 tests/test-create-usb.sh
 tests/test-find-remotes.sh
 tests/test-fsck-collections.sh
 tests/test-include-ostree-h.c
 tests/test-init-collections.sh
 tests/test-mock-gio.c
 tests/test-mock-gio.h
 tests/test-prune-collections.sh
 tests/test-pull-collections.sh
 tests/test-pull-override-url.sh
 tests/test-pull-sizes.sh
 tests/test-remote-add-collections.sh
 tests/test-repo-finder-avahi.c
 tests/test-repo-finder-config.c
 tests/test-repo-finder-mount-integration.sh
 tests/test-repo-finder-mount.c
 tests/test-repo.c
 tests/test-summary-update.sh
 tests/test-summary-view.sh
Copyright: 2016-2019, Endless Mobile, Inc.
License: LGPL-2+

Files: tests/test-admin-deploy-none.sh
Copyright: 2019, Robert Fairley <rfairley@redhat.com>
License: LGPL-2+

Files: tests/test-admin-deploy-uboot.sh
Copyright: no-info-found
License: LGPL-2+

Files: tests/test-admin-gpg.sh
Copyright: 2019, Rafael Fonseca <r4f4rfs@gmail.com>
License: LGPL-2+

Files: tests/test-admin-instutil-set-kargs.sh
Copyright: no-info-found
License: LGPL-2+

Files: tests/test-auto-summary.sh
Copyright: no-info-found
License: LGPL-2+

Files: tests/test-commit-sign.sh
 tests/test-pull-resume.sh
Copyright: 2013, Jeremy Whiting <jeremy.whiting@collabora.com>
License: LGPL-2+

Files: tests/test-config.sh
Copyright: 2018, Sinny Kumari <skumari@redhat.com>
License: LGPL-2+

Files: tests/test-fsck-delete.sh
Copyright: 2019, Wind River Systems, Inc.
License: LGPL-2+

Files: tests/test-gpg-signed-commit.sh
Copyright: no-info-found
License: LGPL-2+

Files: tests/test-help.sh
Copyright: 2014, Owen Taylor <otaylor@redhat.com>
License: LGPL-2+

Files: tests/test-local-pull-depth.sh
Copyright: 2015, Dan Nicholson <nicholson@endlessm.com>
License: LGPL-2+

Files: tests/test-local-pull.sh
 tests/test-parent.sh
Copyright: 2014, 2016, Alexander Larsson <alexl@redhat.com>
License: LGPL-2+

Files: tests/test-pre-signed-pull.sh
 tests/test-signed-commit.sh
 tests/test-signed-pull-summary.sh
 tests/test-signed-pull.sh
Copyright: 2019, 2020, Collabora Ltd.
License: LGPL-2+

Files: tests/test-pull-untrusted.sh
Copyright: no-info-found
License: LGPL-2+

Files: tests/test-refs-collections.sh
 tests/test-summary-collections.sh
Copyright: no-info-found
License: LGPL-2+

Files: tests/test-remote-cookies.sh
Copyright: no-info-found
License: LGPL-2+

Files: tests/test-remotes-config-dir.js
Copyright: no-info-found
License: LGPL-2+

Files: aclocal.m4 apidoc/html/* buildutil/* buildutil/ltversion.m4 libglnx/* src/libostree/ostree-soup-form.c src/libostree/ostree-soup-uri.c src/libostree/ostree-soup-uri.h src/libostree/ostree-soup-uri.c src/libostree/ostree-soup-uri.h tests/fah-deltadata-new.tar.xz tests/fah-deltadata-old.tar.xz tests/gpg-verify-data/* tests/gpg-verify-data/lgpl2 tests/gpg-verify-data/lgpl2.sig1 tests/gpg-verify-data/lgpl2.sig2 tests/gpg-verify-data/lgpl2.sig3 tests/gpg-verify-data/lgpl2.sig4 tests/gpg-verify-data/pubring.gpg tests/gpg-verify-data/secring.gpg tests/gpghome/* tests/gpghome/secring.gpg tests/ostree-path-traverse.tar.gz tests/pre-endian-deltas-repo-big.tar.xz tests/pre-endian-deltas-repo-little.tar.xz tests/pre-signed-pull-data.tar.gz
Copyright:
 © 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 © 1995-2015 Free Software Foundation, Inc.
 © 1999-2003 Ximian, Inc.
 © 2008-2020 Red Hat, Inc
 © 2011-2019 Colin Walters <walters@verbum.org>
 © 2011 Avery Pennarun
 © 2013-2020 Collabora Ltd.
 © 2013 Stef Walter <stef@redhat.com>
 © 2013 Javier Martinez <javier.martinez@collabora.co.uk>
 © 2013 Jeremy Whiting <jeremy.whiting@collabora.com>
 © 2013-2016 Sjoerd Simons <sjoerd.simons@collabora.co.uk>
 © 2014-2017 Alexander Larsson <alexl@redhat.com>
 © 2014 Anne LoVerso
 © 2014 Owen Taylor <otaylor@redhat.com>
 © 2015 Dan Nicholson <nicholson@endlessm.com>
 © 2015 Canonical Ltd.
 © 2015 Matthew Barnes
 © 2016-2019 Endless Mobile, Inc.
 © 2016 Kinvolk GmbH
 © 2017 Georges Basile Stavracas Neto
 © 2018 Matthew Leeds
 © 2018 Sinny Kumari
 © 2019 Rafael Fonseca
 © 2019 Robert Fairley
 © 2019 Wind River Systems, Inc.
 © 2019 Denis Pynkin
License: LGPL-2+

Files: bsdiff/README.md
Copyright:
 © 2003-2005 Colin Percival
 © 2012 Matthew Endsley
 © 2015 Giuseppe Scrivano
License: BSD-2-clause
